// SPDX-License-Identifier: BSD-3-Clause
// SPDX-FileCopyrightText: 2020 Thomas Fischer

let regularExpressionEmailAddress = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/i;
let regularExpressionLowercaseLetter = /[a-z]/;
let regularExpressionUppercaseLetter = /[A-Z]/;
let regularExpressionDigit = /[0-9]/;
// Anything except for digits and letters
let regularExpressionSpecialChar = /[^0-9a-zA-Z]/;

function testLooksLikePassword(text) {
    var lookslikepassword = false;
    var textlength = text.length
    if (text && textlength >= 5 && !text.includes(" ") && !regularExpressionEmailAddress.test(text)) {
        // So, 'text' is a non-empty string of length at least 5 characters without spaces and which does not look like an email address

        // Count the occurrence of different character/symbol classes
        var countLowercaseLetters = (text.match(regularExpressionLowercaseLetter) || []).length;
        var countUppercaseLetters = (text.match(regularExpressionUppercaseLetter) || []).length;
        var countDigits = (text.match(regularExpressionDigit) || []).length;
        var countSpecialChars = (text.match(regularExpressionSpecialChar) || []).length;

        // Is a text if it either
        // - contains both lowercase and uppercase letters and digits
        // - contains a special character and two out of three of the group of lowercase and uppercase letters and digits
        // - is at least 8 characters long and contains a special character and one out of three of the group of lowercase and uppercase letters and digits
        // then assume it is a password
        lookslikepassword = (countLowercaseLetters > 0 && countUppercaseLetters > 0 && countDigits > 0) ||
            (countSpecialChars > 0 && ((countLowercaseLetters > 0 && countUppercaseLetters > 0) || (countLowercaseLetters > 0 && countDigits > 0) || (countUppercaseLetters > 0 && countDigits)) ||
                (textlength >= 8 && countSpecialChars > 0 && (countLowercaseLetters > 0 || countDigits > 0 || countUppercaseLetters > 0))
            );
    }

    return lookslikepassword;
}

function checkTextInputForPassword(input) {
    var text = input.value;
    var lookslikepassword = testLooksLikePassword(text);

    // Change the CSS of the input field depending on the assessment whether it contains a password or not
    input.style.color = lookslikepassword ? "#ffc" : "";
    input.style.background = lookslikepassword ? "#c00" : "";
    input.style.border = lookslikepassword ? "solid 2px #fc3" : "";

    // Set or remove attribute 'lookslikepassword' in case of suspicion of containing a password
    if (lookslikepassword) {
        input.setAttribute("lookslikepassword", "true");
    } else {
        input.removeAttribute("lookslikepassword");
    }

    // Also, update the window/tab's title to contain a warning if the text field is assumed to contain a password
    let warningInTitle = "POTENTIAL PASSWORD LEAKAGE *** ";
    if (lookslikepassword && !document.title.startsWith(warningInTitle)) {
        document.title = warningInTitle + document.title;
    } else if (!lookslikepassword && document.title.startsWith(warningInTitle)) {
        document.title = document.title.substr(warningInTitle.length);
    }
}

function checkTextInputForPasswordEvent(event) {
    checkTextInputForPassword(event.currentTarget);
}

function installListeners() {
    // For all forms in a webpage ...
    for (f = 0; f < document.forms.length; f++) {
        // Keep track whether to intercept form submission
        var interceptformsubmission = false;

        for (i = 0; i < document.forms[f].elements.length; i++) {
            if (document.forms[f].elements[i].type === "text" || document.forms[f].elements[i].type === "email") {
                // Remove previous event listener if any (TODO: necessary?)
                document.forms[f].elements[i].removeEventListener("input", checkTextInputForPasswordEvent);
                // Add event listener to check field's changing value if it looks like a password
                document.forms[f].elements[i].addEventListener("input", checkTextInputForPasswordEvent);

                // As soon as a form contains a 'text' or 'email' field, remember to intercept form submission
                interceptformsubmission = true;
            }
        }

        if (interceptformsubmission) {
            document.forms[f].onsubmit = function(event) {
                event.preventDefault();
                var form = event.currentTarget;
                var stopsubmission = false;
                for (i = 0; !stopsubmission && i < form.elements.length; i++) {
                    if (form.elements[i].hasAttribute("lookslikepassword") || form.elements[i].getAttribute("lookslikepassword") === "true") {
                        stopsubmission |= !confirm("On of the fields in the form to be submitted looks like a password.\n\nSubmit anyway?");
                    }
                }
                if (!stopsubmission) {
                    event.currentTarget.submit();
                }
                return true;
            };
        }
    }
}

function checkAllTextInput() {
    for (f = 0; f < document.forms.length; f++) {
        for (i = 0; i < document.forms[f].elements.length; i++) {
            if (document.forms[f].elements[i].type === "text" || document.forms[f].elements[i].type === "email") {
                checkTextInputForPassword(document.forms[f].elements[i]);
            }
        }
    }
}

// Immediately install listeners to react on user input in text fields ...
installListeners();
// ... as well check the default values of text fields directly
checkAllTextInput();

// With some delay to allow for page's complete initalization, perform a check of password containment
// on all input fields and install event listeners, again (some pages like DuckDuckGo need that)
setTimeout(function() {
    installListeners();
    checkAllTextInput();
}, 3000);
