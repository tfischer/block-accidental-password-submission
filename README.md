# Block Accidental Password Submission

This source code is an add-on/extension for Firefox and Chrome/Chromium to detect passwords entered in plain-text form fields and block their submission.

This add-on works as follows:

 1. Monitor plain-text input fields, typically used for email addresses or usernames for text that looks like a password.
 1. Change the color of plain-text input fields that are suspected containing a password to red and yellow to give visual feedback while the user still types, even before submission of the form itself.
 1. Block the submission if the form to be submitted contains a plain-text input field that is suspected to contain a password.
    The user is asked to either confirm or cancel the submission.

## Installing the Add-on

At the time of writing, this add-on has not be published in any of the large browsers' add-on collections.
Still, many browsers allow to manually install add-ons as 'developer'.

 * For Chrome/Chromium, got to the following webpage, scroll to section "Create the Manifest", skip to the three-point item list starting with "Open the Extension Management page".

    > https://developer.chrome.com/extensions/getstarted

 * For Firefox, go to the following webpage, scroll to section "Trying it out", skip to the subsection "Installing" and follow the instructions.
 
    > https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Your_first_WebExtension

## Recognizing Passwords

In order to block the submission of passwords, those must be recognized first.
This add-on does not save your passwords or check any password wallets/vaults.
Instead, it is analyzes text on the contained characters to assess whether the text is a password or something else.

This assessment is not perfect.
Some passwords, especially simple or short ones, will not be recognized as passwords and thus not block.
Regular, non-password texts may be falsely recognized as passwords if they match the criteria as encoded in the code.

The criteria used to recognize password will likely change over time in order to reduce the number of false negatives (password not recognized as such) and false positives (text recognized as password even if it is not).
If you want to contribute to or improve the criteria used for recognizing passwords, please have a look at the following _Development_ section.

## Development

This add-on's source code is free software published under the so-called 3-clause BSD open source license.
The source code is availabe at GitLab:

> https://gitlab.com/tfischer/block-accidental-password-submission/

Contributions and bug reports are welcome and may be submitted through the GitLab page.
